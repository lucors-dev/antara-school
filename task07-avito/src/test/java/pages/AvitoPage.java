package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

/**
 * Класс страницы Авито.<br>
 * Реализует текучий интерфейс.
 */
public class AvitoPage {
    private final WebDriver driver;
    private final WebDriverWait wait;
    private final By locatorCategory = By.id("category");
    private final By locatorSearchItem = By.id("downshift-input");
    private final By locatorCitySet = By.xpath(".//*[@data-marker='search-form/region']");
    private final By locatorCityName = By.xpath(".//*[@data-marker='popup-location/region/input']");
    private final By locatorCitySave = By.xpath(".//*[@data-marker='popup-location/save-button']");
    private final By locatorFilterDelivery = By.xpath(".//*[@data-marker='delivery-filter/text']");
    private final By locatorFilterPhoto = By.xpath(".//*[@data-marker='search-form/with-images']");
    private final By locatorFilterSubmit = By.xpath(".//*[@data-marker='search-filters/submit-button']");
    private final By locatorFilterOrder = By.cssSelector(".index-topPanel-McfCA .select-select-IdfiC");


    public AvitoPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, Duration.ofSeconds(5));
    }

    public WebDriver getDriver() {
        return this.driver;
    }

    public AvitoPage open() {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("https://www.avito.ru/");
        return this;
    }

    public AvitoPage selectCategory(String visibleText) {
        //Выбор категории на Авито зависит от версии, переданной клиенту
        try {
            // Лучший вариант, когда выбор категории выполнен в виде Select элемента
            WebElement categorySelect = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCategory));
            new Select(categorySelect).selectByVisibleText(visibleText);
        } catch (Exception ex) {
            // Худший, если на странице новый дизайн, отображающий подкатегории только при просмотре родительских категорий
            By locatorPopupButton = By.cssSelector(".top-rubricator-rubricatorButton-SoKyQ");
            WebElement popupButton = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorPopupButton));
            popupButton.click();

            By locatorAllCategories = By.cssSelector(".new-rubricator-content-leftcontent-my5E0 > *");
            List<WebElement> categories = driver.findElements(locatorAllCategories);
            By locatorCategoryAlt = By.xpath(".//*[@data-name='" + visibleText + "']");
            // Вариант рабочий, однако чрезвычайно долгий
            for (WebElement category : categories) {
                wait.withTimeout(Duration.ofSeconds(0));
                category.click();
                try {
                    WebElement categorySelect = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCategoryAlt));
                    categorySelect.click();
                    wait.withTimeout(Duration.ofSeconds(10));
                    break;
                } catch (Exception ignored) {
                }
            }
        }
        return this;
    }

    public AvitoPage inputSearchItem(String itemName) {
        WebElement searchItemInput = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorSearchItem));
        searchItemInput.sendKeys(Keys.CONTROL + "a");
        searchItemInput.sendKeys(Keys.DELETE);
        searchItemInput.sendKeys(itemName);
        return this;
    }

    public AvitoPage openCityPopup() {
        WebElement setCityButton = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCitySet));
        setCityButton.click();
        return this;
    }

    public AvitoPage setCity(String cityName) {
        WebElement cityNameInput = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCityName));
        //Очищаем поля перед непосредственным вводом
        cityNameInput.sendKeys(Keys.CONTROL + "a");
        cityNameInput.sendKeys(Keys.DELETE);
        cityNameInput.sendKeys(cityName);
        //Выбор догадки Авито, поскольку одного лишь ввода в поле недостаточно
        By locatorCitySuggest = By.xpath(
                "(.//strong[text()='" + cityName + "']/parent::*/parent::*[contains(text(), ',')])[1]"
        );
        WebElement citySuggest = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCitySuggest));
        citySuggest.click();
        return this;
    }

    public AvitoPage saveCityAndSearch() {
        WebElement citySaveButton = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorCitySave));
        citySaveButton.click();
        return this;
    }

    public AvitoPage enableDelivery() {
        //Здесь и далее, когда необходимо проверить чекбокс проверяется наличие составного класса "checked"
        try {
            driver.findElement(
                    By.xpath(".//*[@data-marker='delivery-filter/text']/parent::" +
                            "*[contains(@class, 'checkbox-checkbox-KO_ws checkbox-size-s-tYC2A " +
                            "checkbox-checked-_eGx7 checkbox-set-W_iAg')]")
            );
        } catch (Exception ex) {
            WebElement checkbox = wait.until(ExpectedConditions.presenceOfElementLocated(locatorFilterDelivery));
            Actions actions = new Actions(driver);
            //Возникают проблемы с использованием WebElement::click, поэтому используется Actions::click
            actions.click(checkbox).perform();
            WebElement filterSubmit = driver.findElement(locatorFilterSubmit);
            filterSubmit.click();
        }
        return this;
    }

    public AvitoPage enablePhoto() {
        //Внимание. Новый дизайн не содержит чекбокс "Только с фото." на странице.
        try {
            driver.findElement(
                    By.xpath(".//*[@name='withImagesOnly']/parent::" +
                            "*[contains(@class, 'checkbox-checkbox-KO_ws checkbox-size-s-tYC2A " +
                            "checkbox-checked-_eGx7 checkbox-set-W_iAg')]")
            );
        } catch (Exception ex) {
            WebElement checkbox = wait.until(ExpectedConditions.presenceOfElementLocated(locatorFilterPhoto));
            Actions actions = new Actions(driver);
            actions.click(checkbox).perform();
        }
        return this;
    }

    public AvitoPage checkItemsPageName(String itemsName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
            By.xpath(".//*[(@data-marker='page-title/text') and contains(text(), '«" + itemsName + "»')]")
        ));
        return this;
    }

    public AvitoPage selectOrder(String visibleText) {
        WebElement orderSelect = wait.until(ExpectedConditions.visibilityOfElementLocated(locatorFilterOrder));
        new Select(orderSelect).selectByVisibleText(visibleText);
        return this;
    }

    public AvitoPage printItemsDesc(int count) {
        for (int i = 1; i < count + 1; i++) {
            try {
                WebElement el = driver.findElement(
                        By.xpath(".//*[@data-marker='item'][" + i + "]")
                );

                String name = el
                        .findElement(By.xpath(".//*[@itemprop='name']"))
                        .getText();
                String price = el
                        .findElement(By.xpath(".//*[@itemprop='price']"))
                        .getAttribute("content");
                System.out.printf("%s[%s]\n", name, price);
            } catch (Exception ignored) {
            }
        }
        return this;
    }
}
