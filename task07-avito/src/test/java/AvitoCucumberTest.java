import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

/**
 * Вторая часть задания.<br>
 * Раннер сценарных тестов
 */
@CucumberOptions(
        features = "src/test/java/features",
        glue = "steps",
        tags = "@AvitoSearch"
)
public class AvitoCucumberTest extends AbstractTestNGCucumberTests {
}
