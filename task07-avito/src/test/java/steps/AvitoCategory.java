package steps;

public enum AvitoCategory {
    оргтехника("Оргтехника и расходники");

    AvitoCategory(String value) {
        this.value = value;
    }

    public String value;

    public String getValue() {
        return value;
    }
}
