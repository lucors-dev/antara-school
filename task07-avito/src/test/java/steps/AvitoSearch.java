package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.ParameterType;
import io.cucumber.java.bg.И;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.qameta.allure.Attachment;


import pages.AvitoPage;
import ru.yandex.qatools.ashot.AShot;

import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class AvitoSearch {
    private WebDriver driver = null;
    private AvitoPage avitoPage;
    private AvitoOrder avitoOrderDataTable;

    @ParameterType(".*")
    public AvitoCategory categories(String category) {
        return AvitoCategory.valueOf(category);
    }

    @ParameterType(".*")
    public AvitoOrder orders(String order) {
        return AvitoOrder.valueOf(order);
    }

    @Step("Открыт сайт Авито")
    @Пусть("открыт ресурс авито")
    public void init() {
        System.setProperty("webdriver.chrome.driver", "C://WebDriver/chromedriver.exe");
        driver = new ChromeDriver();
        avitoPage = new AvitoPage(driver);
        avitoPage.open();
        captureScreenShot();
    }

    @Step("Выбрана категория '{category.value}'")
    @И("в выпадающем списке категорий выбрана {categories}")
    public void selectCategory(AvitoCategory category) {
        avitoPage.selectCategory(category.getValue());
        captureScreenShot();
    }

    @Step("В поле поиска введено '{itemName}'")
    @И("в поле поиска введено значение {word}")
    public void inputSearchItem(String itemName) {
        avitoPage.inputSearchItem(itemName);
        captureScreenShot();
    }

    @Step("Активирован чекбокс 'Только с фотографией'")
    @И("активирован чекбокс только с фотографией")
    public void enablePhoto() {
        avitoPage.enablePhoto();
        captureScreenShot();
    }

    @Step("Открыт список регионов")
    @Тогда("кликнуть по выпадающему списку региона")
    public void openCityPopup() {
        avitoPage.openCityPopup();
        captureScreenShot();
    }

    @Step("Выбран город '{cityName}'")
    @Тогда("в поле регион введено значение {word}")
    public void setCity(String cityName) {
        avitoPage.setCity(cityName);
        captureScreenShot();
    }

    @Step("Нажата кнопка показать объявления")
    @И("нажата кнопка показать объявления")
    public void saveCityAndSearch() {
        avitoPage.saveCityAndSearch();
        captureScreenShot();
    }

    @Step("Открыта страница результатов")
    @Тогда("открылась страница результаты по запросу {word}")
    public void checkItemsPageName(String itemsName) {
        avitoPage.checkItemsPageName(itemsName);
        captureScreenShot();
    }

    @Step("Выбрана сортировка")
    @И("в выпадающем списке сортировка выбрана значение {orders}")
    public void selectOrder(AvitoOrder order) {
        avitoPage.selectOrder(order.getValue());
        captureScreenShot();
    }

    @Step("В консоль выведено значение название и цены {count} первых товаров")
    @И("в консоль выведено значение название и цены {int} первых товаров")
    public void printItemsDesc(int count) {
        avitoPage.printItemsDesc(count);
        driver.quit();
    }

    @И("для сортировки выбрано значение {orders}")
    public void selectOrderDataTable(AvitoOrder order) {
        avitoOrderDataTable = order;
    }

    @Step("Результат datatable")
    @Тогда("найдем товары по следующей схеме")
    public void searchByDataTable(DataTable table) {
        List<Map<String, String>> dataTable = table.asMaps();

        for (Map<String, String> row : dataTable) {
            avitoPage
                    .inputSearchItem(row.get("Наименование"))
                    .openCityPopup()
                    .setCity(row.get("Город"))
                    .saveCityAndSearch()
                    .selectOrder(avitoOrderDataTable.getValue())
                    .printItemsDesc(Integer.parseInt(row.get("Количество")));
        }
        driver.quit();
    }

    @Attachment(value = "Attachment Screenshot", type = "image/png")
    private byte[] captureScreenShot() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] imageInByte = null;
        try {
            ImageIO.write(new AShot().takeScreenshot(driver).getImage(), "png", baos);
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageInByte;
    }

}
