package steps;

public enum AvitoOrder {
    дороже("Дороже");

    AvitoOrder(String value) {
        this.value = value;
    }

    public String value;

    public String getValue() {
        return value;
    }
}
