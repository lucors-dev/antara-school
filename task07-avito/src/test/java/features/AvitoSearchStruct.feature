#language: ru
#Дополнительное задание: стркутурный сценарий
@AvitoSearchStruct
Функционал:  Поиск на авито

  Структура сценария: Найдем самые дорогие товары на авито по структуре
    Пусть открыт ресурс авито
    И в выпадающем списке категорий выбрана оргтехника
    И в поле поиска введено значение <Наименование>
    И активирован чекбокс только с фотографией
    Тогда кликнуть по выпадающему списку региона
    Тогда в поле регион введено значение <Город>
    И нажата кнопка показать объявления
    Тогда открылась страница результаты по запросу <Наименование>
    И в выпадающем списке сортировка выбрана значение дороже
    И в консоль выведено значение название и цены <Количество> первых товаров

    Примеры:
      | Наименование  | Город       | Количество  |
      | Принтер       | Владивосток | 3           |
      | Патифон       | Саратов     | 5           |