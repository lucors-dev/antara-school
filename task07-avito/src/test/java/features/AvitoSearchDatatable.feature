#language: ru
#Тестовый сценарий с datatable
@AvitoSearchDT
Функционал:  Поиск на авито

  Сценарий: Найдем самые дорогие товары на авито по таблице
    Пусть открыт ресурс авито
    И для сортировки выбрано значение дороже
    Тогда найдем товары по следующей схеме
      | Наименование  | Город       | Количество  |
      | Принтер       | Владивосток | 3           |
      | Патифон       | Саратов     | 5           |
