package petstore;

import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.response.ValidatableResponse;
import model.Order;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

public class StoreTest {
    private static final String BASE_URI = "https://petstore.swagger.io/v2/";
    private static final String ENDPOINT_BASE = "/store";
    private static final String ENDPOINT_ORDER = ENDPOINT_BASE + "/order";
    private static final String ENDPOINT_ORDER_ID = ENDPOINT_ORDER + "/{orderId}";
    private static final String ENDPOINT_INVENTORY = ENDPOINT_BASE + "/inventory";

    @BeforeClass
    public void prepareClass() {
        Properties prop = new Properties();
        String propertiesPath = "src/main/resources/petstore.properties";
        try {
            InputStream input = Files.newInputStream(Paths.get(propertiesPath));
            prop.load(input);
        } catch (Exception ex) {
            System.out.println("Props. read error:");
            ex.getStackTrace();
            return;
        }

        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(BASE_URI)
                .addHeader("api_key", prop.getProperty("api_key"))
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .build();
    }

    public Order createOrder() {
        Order dummyOrder = new Order();
        dummyOrder.setId(new Random().nextInt(10));
        dummyOrder.setPetId(new Random().nextInt(100));
        dummyOrder.setQuantity(new Random().nextInt(100));
        return dummyOrder;
    }

    public ValidatableResponse postOrder(Order dummyOrder) {
        deleteOrder(dummyOrder.getId()); //Уверены что ранее такого Order не было
        return given()
                .body(dummyOrder)
                .when()
                    .post(ENDPOINT_ORDER)
                .then();
    }

    public ResponseBodyExtractionOptions getOrder(int orderId) {
        return given()
                .pathParam("orderId", orderId)
                .when()
                    .get(ENDPOINT_ORDER_ID)
                .then()
                    .assertThat().statusCode(200)
                    .extract().body();
    }

    public ValidatableResponse deleteOrder(int orderId) {
       return given()
                .pathParam("orderId", orderId)
                .when()
                    .delete(ENDPOINT_ORDER_ID)
                .then();
    }


    @Test
    public void placeOrderTest() {
        Order dummyOrder = createOrder();
        postOrder(dummyOrder)
                .assertThat().statusCode(200);
    }

    @Test
    public void findOrderTest() {
        Order dummyOrder = createOrder();
        postOrder(dummyOrder).assertThat().statusCode(200);
        Order foundOrder = getOrder(dummyOrder.getId()).as(Order.class);
        Assert.assertEquals(dummyOrder, foundOrder);
    }

    @Test
    public void deleteOrderTest() {
        Order dummyOrder = createOrder();
        postOrder(dummyOrder)
                .assertThat().statusCode(200);
        deleteOrder(dummyOrder.getId())
                .assertThat().statusCode(200);

        given()
                .pathParam("orderId", dummyOrder.getId())
                .when()
                    .get(ENDPOINT_ORDER_ID)
                .then()
                    .assertThat().statusCode(404);
    }

    @Test
    public void inventoryOrderTest() {
        Map inventory = given()
                .when()
                    .get(ENDPOINT_INVENTORY)
                .then()
                    .assertThat().statusCode(200)
                    .extract().body().as(Map.class);
        Assert.assertTrue(inventory.containsKey("sold"), "Inventory не содержит статус sold" );
    }
}
