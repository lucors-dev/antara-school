package model;

public class Kotik {
    private static int instancesCount; //default 0
    private int satiety; //default 0
    private int prettiness;
    private String name;
    private int weight;
    private String meow;

    public Kotik() {
        Kotik.instanceAdd();
    }

    public Kotik(int prettiness, String name, int weight, String meow) {
        this();
        //Наименования полей класса взято из текста дз
        setKotik(prettiness, name, weight, meow);
    }

    // Сигнатура setKotik взята из текста дз
    public void setKotik(int prettiness, String name, int weight, String meow) {
        setPrettiness(prettiness);
        setName(name);
        setWeight(weight);
        setMeow(meow);
    }

    public static int getInstancesCount() {
        return Kotik.instancesCount;
    }

    private static void instanceAdd() {
        Kotik.instancesCount++;
    }

    protected void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }

    public void printSatiety() {
        System.out.printf("Сытость: %d\n", getSatiety());
    }

    public int getPrettiness() {
        return prettiness;
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeow() {
        return meow;
    }

    public void setMeow(String meow) {
        this.meow = meow;
    }

    //Behavior below
    public void eat(int satiety) {
        setSatiety(getSatiety() + satiety);
        System.out.print("Котик поел. ");
        printSatiety();
    }

    public void eat(int satiety, String foodName) {
        System.out.printf("Котик кушает %s. ", foodName);
        eat(satiety);
    }

    public void eat() {
        eat(4, "Вискас");
    }

    public void activityInfluence() {
        satiety--;
        printSatiety();
    }

    public boolean checkHungry() {
        return checkHungry(false);
    }

    public boolean checkHungry(boolean mute) {
        if (getSatiety() > 0) return false;
        if (!mute) System.out.print("Кот голоден! ");
        return true;
    }

    public boolean play() {
        if (checkHungry()) return false;
        System.out.print("Кот играется! ");
        activityInfluence();
        return true;
    }

    public boolean sleep() {
        if (checkHungry()) return false;
        System.out.print("Кот решил поспать. ");
        activityInfluence();
        return true;
    }

    public boolean chaseMouse() {
        if (checkHungry()) return false;
        System.out.print("Кот охотится на мышей... ");
        if ((int) (Math.random() * 2 + 1) > 1) {
            System.out.print("Удача! ");
            eat(1, "Мышь");
        } else {
            System.out.print("Провал! ");
            activityInfluence();
        }
        return true;
    }

    public boolean walkAround() {
        if (checkHungry()) return false;
        System.out.print("Кот пошел на прогулку. ");
        activityInfluence();
        return true;
    }

    public boolean staring() {
        if (checkHungry()) return false;
        System.out.print("Кот смотрит в окно. ");
        activityInfluence();
        return true;
    }

    public void liveAnotherDay() {
        for (short i = 1; i <= 24; i++) {
            System.out.printf("%d -- ", i);
            int methodNum = (int) (Math.random() * 5 + 1);
            boolean isFed = true;
            switch (methodNum) {
                case 1:
                    isFed = play();
                    break;
                case 2:
                    isFed = sleep();
                    break;
                case 3:
                    isFed = chaseMouse();
                    break;
                case 4:
                    isFed = walkAround();
                    break;
                case 5:
                    isFed = staring();
                    break;
            }
            if (!isFed) eat();
        }
    }
}
