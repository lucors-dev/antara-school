import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik cat1 = new Kotik(100, "Йосик", 3, "мя");
        Kotik cat2 = new Kotik();
        cat2.setKotik(98, "Дымок", 2, "миу");

        cat1.liveAnotherDay();
        System.out.println();
        System.out.printf("Кот1: Имя=%s, Вес=%d, Милый=%d\n", cat1.getName(), cat1.getWeight(), cat1.getPrettiness());
        System.out.printf("Котики мяукают одинаково? %b\n", cat1.getMeow().equals(cat2.getMeow()));
        System.out.printf("Котиков создано: %d\n", Kotik.getInstancesCount());
    }
}
