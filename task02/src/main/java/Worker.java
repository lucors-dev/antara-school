import animals.Animal;
import animals.behavior.Voice;
import food.Food;

/**
 * Класс рабочего зоопарка
 */
public class Worker {
    /**
     * Метод кормления животного
     *
     * @param animal животное
     * @param food   подходящая еда
     */
    public void feed(Animal animal, Food food) {
        animal.eat(food);
    }

    /**
     * Метод, заставляющий животное подать голос
     *
     * @param animal животное, способное подавать голос
     */
    public void getVoice(Voice animal) {
        if (!(animal instanceof Animal)) {
            System.out.println("Не животное.");
            return;
        }
        System.out.printf("%s сказал(а) %s\n", ((Animal) animal).getName(), animal.voice());
    }
}
