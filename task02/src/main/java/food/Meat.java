package food;

/**
 * Класс еды для плотоядного
 */
public class Meat extends Food {
    /**
     * Конструктор мяса
     *
     * @param name    наименование еды
     * @param satiety степень сытости мяса
     */
    public Meat(String name, int satiety) {
        this.name = name;
        this.satiety = satiety;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getSatiety() {
        return this.satiety;
    }
}
