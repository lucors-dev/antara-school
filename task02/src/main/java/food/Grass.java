package food;

/**
 * Класс еды для травоядного
 */
public class Grass extends Food {
    /**
     * Конструктор травы
     *
     * @param name    наименование еды
     * @param satiety степень сытости мяса
     */
    public Grass(String name, int satiety) {
        this.name = name;
        this.satiety = satiety;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getSatiety() {
        return this.satiety;
    }
}
