package food;

/**
 * Абстрактный класс потребляемой еды
 */
public abstract class Food {
    /**
     * Наименование еды
     */
    protected String name;
    /**
     * Уровень сытости еды
     */
    protected int satiety;

    /**
     * Метод получения наименования еды
     *
     * @return наименование еды
     */
    public abstract String getName();

    /**
     * Метод получения уровеня сытости еды
     *
     * @return уровень сытости еды
     */
    public abstract int getSatiety();
}
