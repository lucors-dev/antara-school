package animals;

import food.Food;

import lombok.Getter;

/**
 * Абстрактный класс животного
 */
@Getter
public abstract class Animal {
    /**
     * Имя животного
     */
    private String name;
    /**
     * Вес животного в кг.
     */
    private double weight;
    /**
     * Возраст животного в полных годах
     */
    private int age;
    /**
     * Уровень сытости животного
     */
    private int satiety;

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + getName() + '\'' +
                ", weight=" + getWeight() +
                ", age=" + getAge() +
                ", satiety=" + getSatiety() +
                '}';
    }

    /**
     * Метод изменения уровня сытости после активности
     *
     * @param power сила активности
     */
    protected void influenceActivityOnSatiety(int power) {
        if (power < 0) power = 0;
        int satiety = getSatiety() - power;
        setSatiety(satiety);
    }

    /**
     * Метод потребления пищи
     *
     * @param food экземпляр еды
     */
    public abstract void eat(Food food);

    /**
     * Метод установки имени животного
     *
     * @param name новое имя животного
     */
    public void setName(String name) {
        if (name.isEmpty()) return;
        this.name = name;
    }

    /**
     * Метод установки веса животного
     *
     * @param weight новый вес животного
     */
    public void setWeight(double weight) {
        if (weight <= 0) return;
        this.weight = weight;
    }

    /**
     * Метод установки возраста животного
     *
     * @param age новый возраст животного
     */
    public void setAge(int age) {
        if (age > 100 || age < 0) return;
        this.age = age;
    }

    /**
     * Метод установки уровня сытости животного
     * <p> Сытость устанавливается поведением eat
     *
     * @param satiety новый уровень сытости
     */
    protected void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    /**
     * Метод проверяет текущий уровень сытости животного
     *
     * @return true, если голоден, иначе false
     */
    public boolean isHungry() {
        return isHungry(false);
    }

    /**
     * Метод проверяет текущий уровень сытости животного
     *
     * @param warn флаг, отобразить предупреждение, если голоден
     * @return true, если голоден, иначе false
     */
    public boolean isHungry(boolean warn) {
        return isHungry(1, warn);
    }

    /**
     * Метод проверяет текущий уровень сытости животного
     * с учетом вычета subtract
     *
     * @param subtract вычитаемое
     * @param warn     флаг, отобразить предупреждение если голоден
     * @return true, если голоден, иначе false
     */
    public boolean isHungry(int subtract, boolean warn) {
        if (getSatiety() - subtract < 0) {
            if (warn) printHungryWarning();
            return true;
        }
        return false;
    }

    /**
     * Метод проверяет, хватит ли сытости у животного
     * для выполнения действия силы power
     *
     * @param power необходимая сила активности
     * @return true, если голоден, иначе false
     */
    public boolean canActivity(int power) {
        return isHungry(power, true);
    }

    /**
     * Метод вывода сообщения о том, что животное голодное
     */
    protected void printHungryWarning() {
        System.out.printf("%s голоден(на)!\n", getName());
    }
}
