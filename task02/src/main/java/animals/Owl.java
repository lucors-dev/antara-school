package animals;

import animals.behavior.Fly;
import animals.behavior.Swim;
import animals.behavior.Voice;

/**
 * Класс совы
 * <p>Совы плотоядные
 * <p>Способны летать, плавать, подавать голос
 */
public class Owl extends Carnivorous implements Fly, Swim, Voice {
    /**
     * Конструктор совы
     */
    public Owl(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
    }

    public void fly() {
        if (canActivity(1)) return;
        System.out.printf("Сова %s взмыла ввысь.\n", getName());
        influenceActivityOnSatiety(1);
    }

    public void swim() {
        if (canActivity(3)) return;
        System.out.printf("Сове %s пришло время поплавать.\n", getName());
        influenceActivityOnSatiety(3);
    }

    public String voice() {
        return "Ухуу";
    }
}
