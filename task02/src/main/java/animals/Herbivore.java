package animals;

import food.Food;
import food.Grass;

/**
 * Абстрактный класс травоядного
 */
public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {
        if (!(food instanceof Grass)) {
            System.out.printf("%s не может съесть %s, он(а) травоядный(ая)\n", getName(), food.getName());
            return;
        }
        System.out.printf("%s поел(а) %s\n", getName(), food.getName());
        setSatiety(getSatiety() + food.getSatiety());
    }
}
