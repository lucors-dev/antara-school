package animals;

import animals.behavior.Run;
import animals.behavior.Swim;
import animals.behavior.Voice;

/**
 * Класс кролика
 * <p>Кролики травоядные
 * <p>Способны плавать, бегать, подавать голос
 */
public class Rabbit extends Herbivore implements Swim, Run, Voice {
    /**
     * Конструктор кролика
     */
    public Rabbit(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
    }

    public void swim() {
        if (canActivity(2)) return;
        System.out.printf("Кролик %s куда-то поплыл.\n", getName());
        influenceActivityOnSatiety(2);
    }

    public void run() {
        if (canActivity(1)) return;
        System.out.printf("Кролик %s бегает вокруг.\n", getName());
        influenceActivityOnSatiety(1);
    }

    public String voice() {
        return "Кррхх";
    }
}
