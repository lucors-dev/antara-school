package animals;

import animals.behavior.Fly;
import animals.behavior.Run;
import animals.behavior.Voice;

/**
 * Класс голубя
 * <p>Голуби травоядные
 * <p>Способны летать, бегать, подавать голос
 */
public class Pigeon extends Herbivore implements Fly, Run, Voice {
    /**
     * Конструктор голубя
     */
    public Pigeon(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
    }

    public void fly() {
        if (canActivity(1)) return;
        System.out.printf("Голубь %s кружит в небе.\n", getName());
        influenceActivityOnSatiety(1);
    }

    public void run() {
        if (canActivity(3)) return;
        System.out.printf("Голубь %s убегает, но не улетает.\n", getName());
        influenceActivityOnSatiety(3);
    }

    public String voice() {
        return "Гурррр";
    }
}
