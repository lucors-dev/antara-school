package animals;

import animals.behavior.Fly;
import animals.behavior.Run;
import animals.behavior.Swim;
import animals.behavior.Voice;

/**
 * Класс утки
 * <p>Утки травоядные
 * <p>Способны плавать, летать, бегать, подавать голос
 */
public class Duck extends Herbivore implements Swim, Fly, Run, Voice {
    /**
     * Конструктор утки
     */
    public Duck(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
    }

    public void swim() {
        if (canActivity(1)) return;
        System.out.printf("Утка %s плавает в водоёме.\n", getName());
        influenceActivityOnSatiety(1);
    }

    public void fly() {
        if (canActivity(2)) return;
        System.out.printf("Утка %s улетела...\n", getName());
        influenceActivityOnSatiety(2);
    }

    public void run() {
        if (canActivity(2)) return;
        System.out.printf("Утка %s бегает по кругу.\n", getName());
        influenceActivityOnSatiety(2);
    }

    public String voice() {
        return "Кря-кря";
    }
}
