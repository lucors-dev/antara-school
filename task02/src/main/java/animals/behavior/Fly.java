package animals.behavior;

/**
 * Интерфейс способного полетать
 */
public interface Fly {
    /**
     * Метод поведения полета
     */
    void fly();
}
