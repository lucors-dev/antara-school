package animals.behavior;

/**
 * Интерфейс способного бегать
 */
public interface Run {
    /**
     * Метод поведения бега
     */
    void run();
}
