package animals.behavior;

/**
 * Интерфейс способного плавать
 */
public interface Swim {
    /**
     * Метод поведения плавания
     */
    void swim();
}
