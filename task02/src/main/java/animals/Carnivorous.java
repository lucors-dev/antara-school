package animals;

import food.Food;
import food.Meat;

/**
 * Абстрактный класс плотоядного
 */
public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
        if (!(food instanceof Meat)) {
            System.out.printf("%s не может съесть %s, он(а) плотоядный(ая)\n", getName(), food.getName());
            return;
        }
        System.out.printf("%s поел(а) %s\n", getName(), food.getName());
        setSatiety(getSatiety() + food.getSatiety());
    }
}
