import food.Food;
import food.Grass;
import food.Meat;

import animals.behavior.Swim;
import animals.Duck;
import animals.Pigeon;
import animals.Rabbit;
import animals.Fish;
import animals.Fox;
import animals.Owl;

import java.util.List;
import java.util.Arrays;

public class Zoo {
    public static void main(String[] args) {
        // Экземпляр работника зоопарка
        Worker worker = new Worker();
        // Экземпляры еды
        Food grass = new Grass("Трава", 1);
        Food ham = new Meat("Мясо", 1);
        // Экземпляры травоядных
        Duck duck = new Duck("Крякуша", 2.1, 2, 4);
        Pigeon pigeon = new Pigeon("Гуля", 0.7, 1, 1);
        Rabbit rabbit = new Rabbit("Кроля", 1.5, 2, 1);
        // Экземпляры плотоядный
        Fish fish = new Fish("Золотушка", 0.2, 1, 1);
        Fox fox = new Fox("Фокси", 2.1, 1, 3);
        Owl owl = new Owl("Совенок", 3, 3, 1);

        // Подача голоса совой
        worker.getVoice(owl);
        //worker.getVoice(fish); //Ошибка Fish не реализует Voice
        // Попытка покормить голубя травой и ветчиной
        worker.feed(pigeon, grass);
        worker.feed(pigeon, ham); //Ошибка Pigeon -- травоядный
        worker.feed(fox, ham);
        worker.feed(fox, grass); //Ошибка Fox -- плотоядный

        // Водоём с плавучими животными
        System.out.println();

        List<Swim> pond = Arrays.asList(fish, duck);
        //pond.add(pigeon); //Ошибка Pigeon не реализует Swim
        pond.forEach(Swim::swim);

        // Вызов оставшихся методов
        System.out.println();
        System.out.println(rabbit);
        duck.run();
        duck.fly();
        System.out.printf("Рыба %s голодная? %b\n", fish.getName(), fish.isHungry());
    }
}
