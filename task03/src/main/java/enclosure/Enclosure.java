package enclosure;

import animals.Animal;

import java.util.HashSet;
import lombok.Getter;

@Getter
public class Enclosure<T extends Animal> {
    /**
     * Множество животных
     */
    private HashSet<T> animals;

    /**
     * Размер вольера
     */
    private EnclosureSize size;

    /**
     * Конструктор вольера
     *
     * @param size размер вольера {@link EnclosureSize}
     */
    public Enclosure(EnclosureSize size) {
        setAnimals(new HashSet<>());
        setSize(size);
    }

    /**
     * Конструктор вольера
     *
     * @param size        размер вольера {@link EnclosureSize}
     * @param copyAnimals копируемое множество животных
     */
    public Enclosure(EnclosureSize size, HashSet<? extends T> copyAnimals) {
        this(size);
        if (copyAnimals != null) {
            copyAnimals.forEach(this::add);
        }
    }

    /**
     * Метод установки размера вольера
     *
     * @param size новое значение размера вольера {@link EnclosureSize}
     * @return Возвращает true, если удалены звери неподходящего размера
     */
    public boolean setSize(EnclosureSize size) {
        this.size = size;
        return getAnimals().removeIf(animal -> !checkAnimalFit(animal));
    }

    /**
     * Метод получения множества животных
     *
     * @return множество animals
     */
    private HashSet<T> getAnimals() {
        return animals;
    }

    /**
     * Метод установки множества животных
     *
     * @param animals множество животных
     */
    private void setAnimals(HashSet<T> animals) {
        this.animals = animals;
    }

    /**
     * Метод проверки поместиться ли животное в вольер
     *
     * @param animal проверяемое животное
     * @return результат проверки
     */
    public boolean checkAnimalFit(T animal) {
        return (animal.getEnclosureSize().ordinal() <= getSize().ordinal());
    }

    /**
     * Метод добавления животного в вольер
     *
     * @param animal добавляемое животное
     * @return результат операции
     */
    public boolean add(T animal) {
        if (!checkAnimalFit(animal)) return false;
        return getAnimals().add(animal);
    }

    /**
     * Метод удаления животного из вольера
     *
     * @param animal удаляемое животное
     * @return результат операции
     */
    public boolean remove(T animal) {
        return getAnimals().remove(animal);
    }

    /**
     * Метод получения животного по его ID
     *
     * @param animalName имя животного
     * @return животное удовлетворяющее запросу или null
     */
    public T getByName(String animalName) {
        for (T animal : getAnimals()) {
            if (animal.getName().equals(animalName)) return animal;
        }
        return null;
    }
}
