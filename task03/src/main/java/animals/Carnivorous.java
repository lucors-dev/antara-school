package animals;

import food.Food;
import food.Meat;
import food.WrongFoodException;

/**
 * Абстрактный класс плотоядного
 */
public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Meat)) {
            throw new WrongFoodException();
        }
        System.out.printf("%s поел(а) %s\n", getName(), food.getName());
        setSatiety(getSatiety() + food.getSatiety());
    }
}
