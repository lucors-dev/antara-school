package animals.behavior;

/**
 * Интерфейс способного подать голос
 */
public interface Voice {
    /**
     * Метод поведения подачи голоса
     *
     * @return строка, голос животного
     */
    String voice();
}
