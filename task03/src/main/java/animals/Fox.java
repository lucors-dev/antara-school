package animals;

import animals.behavior.Run;
import animals.behavior.Swim;
import animals.behavior.Voice;
import enclosure.EnclosureSize;

/**
 * Класс лисы
 * <p>Лисы плотоядные
 * <p>Способны бегать, плавать, подавать голос
 */
public class Fox extends Carnivorous implements Run, Swim, Voice {
    /**
     * Конструктор лисы
     */
    public Fox(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
        setEnclosureSize(EnclosureSize.NORMAL);
    }

    public void run() {
        if (canActivity(1)) return;
        System.out.printf("Лиса %s резко убежала.\n", getName());
        influenceActivityOnSatiety(1);
    }

    public void swim() {
        if (canActivity(2)) return;
        System.out.printf("Лиса %s плавает в поисках добычи.\n", getName());
        influenceActivityOnSatiety(2);
    }

    public String voice() {
        return "Фыр-фыр-фыр";
    }
}
