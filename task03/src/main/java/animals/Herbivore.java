package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

/**
 * Абстрактный класс травоядного
 */
public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) throws WrongFoodException {
        if (!(food instanceof Grass)) {
            throw new WrongFoodException();
        }
        System.out.printf("%s поел(а) %s\n", getName(), food.getName());
        setSatiety(getSatiety() + food.getSatiety());
    }
}
