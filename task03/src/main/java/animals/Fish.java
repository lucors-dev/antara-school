package animals;

import animals.behavior.Swim;
import enclosure.EnclosureSize;

/**
 * Класс рыбы
 * <p>Рыбы плотоядные
 * <p>Способны плавать
 */
public class Fish extends Carnivorous implements Swim {
    /**
     * Конструктор рыбы
     */
    public Fish(String name, double weight, int age, int satiety) {
        setName(name);
        setWeight(weight);
        setAge(age);
        setSatiety(satiety);
        setEnclosureSize(EnclosureSize.TINY);
    }

    public void swim() {
        if (canActivity(1)) return;
        System.out.printf("Рыба %s начала плавать, как и все рыбы.\n", getName());
        influenceActivityOnSatiety(1);
    }
}
