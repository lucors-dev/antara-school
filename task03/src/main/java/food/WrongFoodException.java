package food;

/**
 * Класс исключение неверного типа еды
 */
public class WrongFoodException extends Exception {
    /**
     * Конструктор исключения
     */
    public WrongFoodException() {
        super("Неподходящий тип еды.");
    }
}
