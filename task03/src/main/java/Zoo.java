import animals.Animal;
import animals.Herbivore;
import animals.Carnivorous;
import animals.behavior.Swim;
import animals.Duck;
import animals.Pigeon;
import animals.Rabbit;
import animals.Fish;
import animals.Fox;
import animals.Owl;

import food.Food;
import food.Grass;
import food.Meat;

import enclosure.Enclosure;
import enclosure.EnclosureSize;

import java.util.List;
import java.util.Arrays;
import java.util.HashSet;

public class Zoo {
    public static void main(String[] args) {
        // Экземпляр работника зоопарка
        Worker worker = new Worker();
        // Экземпляры еды
        Food grass = new Grass("Трава", 1);
        Food ham = new Meat("Мясо", 1);
        // Экземпляры травоядных
        Duck duck = new Duck("Крякуша", 2.1, 2, 4);
        Pigeon pigeon = new Pigeon("Гуля", 0.7, 1, 1);
        Rabbit rabbit = new Rabbit("Кроля", 1.5, 2, 1);
        // Экземпляры плотоядный
        Fish fish = new Fish("Золотушка", 0.2, 1, 1);
        Fox fox = new Fox("Фокси", 2.1, 1, 3);
        Owl owl = new Owl("Совенок", 3, 3, 1);

        // Подача голоса совой
        worker.getVoice(owl);
        //worker.getVoice(fish); //Ошибка Fish не реализует Voice
        // Попытка покормить голубя травой и ветчиной
        worker.feed(pigeon, grass);
        worker.feed(pigeon, ham); //Ошибка Pigeon -- травоядный
        worker.feed(fox, ham);
        worker.feed(fox, grass); //Ошибка Fox -- плотоядный

        // Водоём с плавучими животными
        System.out.println();

        List<Swim> pond = Arrays.asList(fish, duck);
        //pond.add(pigeon); //Ошибка Pigeon не реализует Swim
        pond.forEach(Swim::swim);

        // Вызов оставшихся методов
        System.out.println();
        System.out.println(rabbit);
        duck.run();
        duck.fly();
        System.out.printf("Рыба %s голодная? %b\n", fish.getName(), fish.isHungry());

        // Task03 проверки
        System.out.println("\nsmallHerbivore.add below");
        Enclosure<Herbivore> smallHerbivore = new Enclosure<>(EnclosureSize.SMALL);
        System.out.println(smallHerbivore.add(pigeon)); //True (Herbivore и EnclosureSize.SMALL)
        System.out.println(smallHerbivore.add(pigeon)); //False (Дубликат)
        System.out.println(smallHerbivore.add(duck)); //False (Herbivore и EnclosureSize.NORMAL)
        System.out.println(smallHerbivore.add(rabbit)); //False (Herbivore и EnclosureSize.NORMAL)
//        System.out.println(smallHerbivore.add(owl)); //False (Carnivorous)
        System.out.println("\nsmallHerbivore.remove below");
        System.out.println(smallHerbivore.remove(pigeon)); //True
        System.out.println(smallHerbivore.remove(duck)); //False (Такого нет)
//        System.out.println(smallHerbivore.remove(owl)); //False (Carnivorous)

        System.out.println("\nsmallCarnivorous.add below");
        HashSet<Carnivorous> smallCarnivorousSet = new HashSet<>();
        smallCarnivorousSet.add(owl); //Скопируется
        smallCarnivorousSet.add(fox); //Проигнорируется при копировании
        Enclosure<Carnivorous> smallCarnivorous = new Enclosure<>(EnclosureSize.SMALL, smallCarnivorousSet);
        System.out.println(smallCarnivorous.add(fish)); //True (Carnivorous и EnclosureSize.TINY)
        System.out.println(smallCarnivorous.add(owl)); //False (Уже есть после копирования)
        System.out.println(smallCarnivorous.add(fox)); //False (Carnivorous, но EnclosureSize.NORMAL)
        System.out.println(smallCarnivorous.setSize(EnclosureSize.BIG)); //False (Никто не удален)
        System.out.println("\nFox fit? " + smallCarnivorous.checkAnimalFit(fox)); //False

        Fish anotherFish = new Fish("Золотушка", 0.2, 1, 0); //Значения арг. совпадают
        System.out.println("fish == anotherFish? " + fish.equals(anotherFish)); //True (fish.name == anotherFish.name)
        System.out.println(smallCarnivorous.add(anotherFish)); //False (Уже имеет с таким именем)

        Animal enclosureFish = smallCarnivorous.getByName(fish.getName());
        System.out.println("fish == enclosureFish? " + fish.equals(enclosureFish)); //True
        //hashCode генерирует одинаковый хеш
        for (int i = 0; i < 4; i++) {
            System.out.println("fish.hashCode=" + enclosureFish.hashCode());
        }
    }
}
