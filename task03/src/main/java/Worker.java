import animals.Animal;
import animals.Herbivore;
import animals.behavior.Voice;

import food.Food;
import food.WrongFoodException;

/**
 * Класс рабочего зоопарка
 */
public class Worker {
    /**
     * Метод кормления животного
     *
     * @param animal животное
     * @param food   подходящая еда
     */
    public void feed(Animal animal, Food food) {
        try {
            animal.eat(food);
        } catch (WrongFoodException wrongFoodException) {
            String message = wrongFoodException.getMessage();
            message += " " + animal.getName();
            if (animal instanceof Herbivore) {
                message += " травоядный(ая).";
            } else {
                message += " плотоядный(ая).";
            }
            System.out.println(message);
        }
    }

    /**
     * Метод, заставляющий животное подать голос
     *
     * @param animal животное, способное подавать голос
     */
    public void getVoice(Voice animal) {
        if (!(animal instanceof Animal)) {
            System.out.println("Не животное.");
            return;
        }
        System.out.printf("%s сказал(а) %s\n", ((Animal) animal).getName(), animal.voice());
    }
}
