import math.ConsoleIOService;
import math.IOService;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.Assert;

@Test
public class ConsoleIOServiceTest {
    private static final String EOL = System.lineSeparator();
    private static final String TEXT_TO_PRINT1 = "Ничто не истинно";
    private static final String TEXT_TO_PRINT2 = "Все дозволено";

    private PrintStream backup;
    private ByteArrayOutputStream bos;
    private IOService ioService;

    @BeforeMethod
    public void setUp() {
        System.out.println(Thread.currentThread().getName());
        backup = System.out;
        bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));
        ioService = new ConsoleIOService();
    }

    @AfterMethod
    public void tearDown() {
        System.setOut(backup);
    }

    @Test
    public void shouldPrintOnlyFirstCreedLine() throws InterruptedException {
        ioService.writeString(TEXT_TO_PRINT1);
        Thread.sleep(1000);
        Assert.assertEquals(TEXT_TO_PRINT1 + EOL, bos.toString());
    }

    @Test
    public void shouldPrintOnlySecondCreedLine() {
        ioService.writeString(TEXT_TO_PRINT2);
        Assert.assertEquals(TEXT_TO_PRINT2 + EOL, bos.toString());
    }
}
