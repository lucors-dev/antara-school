import math.Calculator;
import math.CalculatorImpl;
import math.CalculatorOperation;
import math.IOService;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.Assert;

import org.mockito.InOrder;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@Test
public class CalculatorImplTest {
    private Calculator calculator;
    private InOrder inOrder;
    private IOService ioService;
    private final String ERROR_PREAMBLE = "Ошибка: ";
    private final String UNKNOWN_OPERATION = "Недопустимая операция";
    private final String UNKNOWN_OPERAND = "Недопустимые операнды";
    private final String ZERO_DIVIDE = "Деление на ноль.";
    private final String CALC_FAIL = "Ошибка вычислений";

    @BeforeMethod
    private void setUp() {
        ioService = mock(IOService.class);
        calculator = new CalculatorImpl(ioService);
        inOrder = inOrder(ioService);
    }

    @DataProvider
    public Object[][] numbersDataProvider() {
        return new Object[][]{
                {"+", "5", "-3", "2.0"},
                {"-", "5", "-3", "8.0"},
                {"*", "5", "-3", "-15.0"},
                {"/", "6", "-3", "-2.0"}
        };
    }

    @DataProvider
    public Object[][] wrongDataProvider() {
        return new Object[][]{
                {"5", "5", "-3"},
                {"-", "Null", "False"},
                {"W", "A", "L"},
                {"/", "3", "0"}
        };
    }

    @Test(dataProvider = "numbersDataProvider")
    public void correctDataReadAndCalc(String op, String firstNum, String secondNum, String result) {
        given(ioService.readString()).willReturn(op);
        CalculatorOperation co = calculator.readOperation();
        Assert.assertNotEquals(null, co);
        inOrder.verify(ioService, times(1)).readString();

        given(ioService.readString()).willReturn(firstNum);
        Double firstOperand = calculator.readOperand();
        Assert.assertEquals(Double.parseDouble(firstNum), firstOperand);
        inOrder.verify(ioService, times(1)).readString();

        given(ioService.readString()).willReturn(secondNum);
        Double secondOperand = calculator.readOperand();
        Assert.assertEquals(Double.parseDouble(secondNum), secondOperand);
        inOrder.verify(ioService, times(1)).readString();

        Assert.assertEquals(Double.parseDouble(result),
                calculator.calc(co, firstOperand, secondOperand));
    }

    @Test(dataProvider = "numbersDataProvider")
    public void fullReadCalc(String op, String firstNum, String secondNum, String result) {
        given(ioService.readString()).willReturn(op).willReturn(firstNum).willReturn(secondNum);
        calculator.readAndCalc();
        inOrder.verify(ioService, times(3)).readString();
        inOrder.verify(ioService, times(1)).writeString(result);
    }


    @Test(dataProvider = "wrongDataProvider")
    public void incorrectDataCalc(String op, String firstNum, String secondNum) {
        given(ioService.readString()).willReturn(op).willReturn(firstNum).willReturn(secondNum);
        calculator.readAndCalc();

        if ("+-/*".indexOf(op.charAt(0)) == -1) {
            inOrder.verify(ioService, times(1)).writeString(ERROR_PREAMBLE + UNKNOWN_OPERATION);
        } else {
            inOrder.verify(ioService, times(3)).readString();
            try {
                Double.parseDouble(firstNum);
                double secondOperand = Double.parseDouble(secondNum);

                if ((secondOperand % 1) == 0) {
                    int zeroChecker = (int) secondOperand;
                    if (zeroChecker == 0) {
                        inOrder.verify(ioService, times(1))
                                .writeString(ZERO_DIVIDE);

                        inOrder.verify(ioService, times(1))
                                .writeString(ERROR_PREAMBLE + CALC_FAIL);
                    }
                }
            } catch (NumberFormatException e) {
                inOrder.verify(ioService, times(1))
                        .writeString(ERROR_PREAMBLE + UNKNOWN_OPERAND);
            }
        }
    }
}
