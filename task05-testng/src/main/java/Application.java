import math.Calculator;
import math.CalculatorImpl;
import math.CalculatorOperation;
import math.ConsoleIOService;

public class Application {
    public static void main(String[] args) {
        Calculator calculator = new CalculatorImpl(new ConsoleIOService());
        calculator.readAndCalc();
        System.out.println(calculator.calc(CalculatorOperation.MULTIPLY, 3.0, 15.0));
    }
}
