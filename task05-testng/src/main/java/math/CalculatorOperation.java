package math;

/**
 * Перечисление доступных операций калькулятора
 */
public enum CalculatorOperation {
    ADD('+'),
    SUBTRACT('-'),
    MULTIPLY('*'),
    DIVIDE('/');

    private final char OP_SYMBOL;

    CalculatorOperation(char operationSymbol) {
        this.OP_SYMBOL = operationSymbol;
    }

    /**
     * Метод получения символа операции
     *
     * @return символ -- символьное представление операции
     */
    public char getCharView() {
        return OP_SYMBOL;
    }
}
