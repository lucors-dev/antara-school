package math;

/**
 * Интерфейс сервиса ввода-вывода
 */
public interface IOService {
    /**
     * Метод вывода сообщения
     *
     * @param message сообщение
     */
    void writeString(String message);

    /**
     * Читает очередную строку с контекста
     *
     * @return строка
     */
    String readString();
}
