package math;

public class CalculatorImpl implements Calculator {
    private final IOService io;

    public CalculatorImpl(IOService io) {
        this.io = io;
    }

    public void readAndCalc() {
        try {
            CalculatorOperation co = readOperation();
            if (co == null)
                throw new Exception("Недопустимая операция");

            Double firstOperand = readOperand();
            Double secondOperand = readOperand();
            if (firstOperand == null || secondOperand == null)
                throw new Exception("Недопустимые операнды");

            Double result = calc(co, firstOperand, secondOperand);
            if (result == null)
                throw new Exception("Ошибка вычислений");

            writeResult(result);
        } catch (Exception e) {
            io.writeString("Ошибка: " + e.getMessage());
        }
    }

    public CalculatorOperation readOperation() {
        String inputLine = io.readString();
        if (inputLine.isEmpty()) return null;

        char opSymbol = inputLine.charAt(0);
        CalculatorOperation[] operations = CalculatorOperation.values();
        for (CalculatorOperation co : operations) {
            if (co.getCharView() == opSymbol) {
                return co;
            }
        }
        return null;
    }

    public Double readOperand() {
        try {
            return Double.parseDouble(io.readString());
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public Double calc(CalculatorOperation operation, Double firstOperand, Double secondOperand) {
        switch (operation) {
            case ADD:
                return add(firstOperand, secondOperand);
            case SUBTRACT:
                return subtract(firstOperand, secondOperand);
            case MULTIPLY:
                return multiply(firstOperand, secondOperand);
            case DIVIDE:
                return divide(firstOperand, secondOperand);
        }
        return null;
    }

    public Double add(Double firstOperand, Double secondOperand) {
        return firstOperand + secondOperand;
    }

    public Double subtract(Double firstOperand, Double secondOperand) {
        return firstOperand - secondOperand;
    }

    public Double multiply(Double firstOperand, Double secondOperand) {
        return firstOperand * secondOperand;
    }

    public Double divide(Double firstOperand, Double secondOperand) {
        if ((secondOperand % 1) == 0) {
            int zeroChecker = secondOperand.intValue();
            if (zeroChecker == 0) {
                io.writeString("Деление на ноль.");
                return null;
            }
        }
        return firstOperand / secondOperand;
    }

    public void writeResult(Double result) {
        io.writeString(result.toString());
    }
}
