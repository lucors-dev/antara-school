package math;

/**
 * Интерфейс калькулятора
 */
public interface Calculator {

    /**
     * Читает поток и производит вычисление. <br>
     * Выводит результат вычислений в IO.
     */
    void readAndCalc();

    /**
     * Читает строку и преобразовывает к CalculatorOperation
     *
     * @return операцию CalculatorOperation
     */
    CalculatorOperation readOperation();

    /**
     * Читает строку и преобразовывает к Number
     *
     * @return число -- потомок Number
     */
    Double readOperand();

    /**
     * Метод вычисляющий результативное значение
     *
     * @param operation     операция CalculatorOperation
     * @param firstOperand  левый операнд Double
     * @param secondOperand правый операнд Double
     * @return число -- потомок Number
     */
    Double calc(CalculatorOperation operation, Double firstOperand, Double secondOperand);

    /**
     * Выводит результат вычислений в IO
     *
     * @param result результат вычислений
     */
    void writeResult(Double result);

    /**
     * Складывает входящие аргументы
     *
     * @return число Double
     */
    Double add(Double firstOperand, Double secondOperand);

    /**
     * Вычисляет разность входящих аргументов
     *
     * @return число Double
     */
    Double subtract(Double firstOperand, Double secondOperand);

    /**
     * Перемножает входящие аргументы
     *
     * @return число Double
     */
    Double multiply(Double firstOperand, Double secondOperand);

    /**
     * Вычисляет частное входящих аргументов
     *
     * @return число Double
     */
    Double divide(Double firstOperand, Double secondOperand);
}
